variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
  default     = "develop"
}

variable "location" {
  type        = string
  description = "Deployment region"
  default     = "eastus"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "domain" {
  type        = string
  description = "Domain name"
}

variable "subdomain" {
  type        = string
  description = "Subdomain name"
}

# Custom sub/domain deployed in cloudflare (project)
variable "domain_on_cloudflare" {
  type        = bool
  default     = false
  description = "Domain on cloudflare"
}

variable "dns_ttl" {
  type        = number
  description = "DNS Time to live"
  default     = 3600
}

variable "cdn_sku" {
  type        = string
  description = "CDN SKU names."
  default     = "Standard_Microsoft"

  validation {
    condition     = contains(["Standard_Akamai", "Standard_Microsoft", "Standard_Verizon", "Premium_Verizon"], var.cdn_sku)
    error_message = "The cdn_sku must be one of the following: Standard_Akamai, Standard_Microsoft, Standard_Verizon, Premium_Verizon."
  }
}

variable "cdn_ttl" {
  type        = string
  description = "Duration of the cache. Only allowed when behavior is set to Override or SetIfMissing. Format: [d.]hh:mm:ss"
  default     = "365.00:00:00"
}

variable "container_name" {
  type        = string
  description = "Storage container name"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
