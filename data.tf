data "azurerm_resource_group" "this" {
  name = var.resource_group_name
}

data "azurerm_dns_zone" "this" {
  name                = var.domain
  resource_group_name = data.azurerm_resource_group.this.name
}

data "cloudflare_zones" "zone" {
  count   = var.domain_on_cloudflare ? 1 : 0

  filter {
    name  = var.domain
  }
}
