terraform {
  required_version = ">=1"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=4.6.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.0"
    }

    time = {
      source  = "hashicorp/time"
      version = ">=0.11"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">=4.30"
    }
  }
}
