locals {
  subdomain_base  = replace(var.subdomain, ".${var.domain}", "")

  tags            = merge(var.tags, {
    source        = "terraform"
    environment   = var.environment
  })
}
