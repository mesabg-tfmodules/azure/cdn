resource "random_string" "identifier" {
  length  = 8
  lower   = true
  numeric = false
  upper   = false
  special = false
}

module "storage" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/azure/blob-storage.git?ref=v1.0.0"
  
  name                = replace(var.name, "-", "")
  container_name      = var.container_name
  environment         = var.environment
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = local.tags
}

resource "azurerm_cdn_profile" "this" {
  name                = "profile-${var.name}"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = var.cdn_sku
  tags                = local.tags
}

resource "azurerm_cdn_endpoint" "this" {
  name                          = "endpoint-${var.name}"
  profile_name                  = azurerm_cdn_profile.this.name
  location                      = var.location
  resource_group_name           = var.resource_group_name
  is_http_allowed               = false
  is_https_allowed              = true
  querystring_caching_behaviour = "IgnoreQueryString"

  optimization_type             = "GeneralWebDelivery"
  global_delivery_rule {
    cache_expiration_action {
      behavior                  = "Override"
      duration                  = var.cdn_ttl
    }

    modify_response_header_action {
      action                    = "Append"
      name                      = "Access-Control-Allow-Origin"
      value                     = "*"
    }

    modify_response_header_action {
      action                    = "Append"
      name                      = "Access-Control-Allow-Methods"
      value                     = "GET"
    }

    modify_response_header_action {
      action                    = "Append"
      name                      = "Access-Control-Allow-Headers"
      value                     = "Content-Type"
    }
  }

  is_compression_enabled        = true
  content_types_to_compress     = [
    "application/eot",
    "application/font",
    "application/font-sfnt",
    "application/javascript",
    "application/json",
    "application/opentype",
    "application/otf",
    "application/pkcs7-mime",
    "application/truetype",
    "application/ttf",
    "application/vnd.ms-fontobject",
    "application/xhtml+xml",
    "application/xml",
    "application/xml+rss",
    "application/x-font-opentype",
    "application/x-font-truetype",
    "application/x-font-ttf",
    "application/x-httpd-cgi",
    "application/x-javascript",
    "application/x-mpegurl",
    "application/x-opentype",
    "application/x-otf",
    "application/x-perl",
    "application/x-ttf",
    "font/eot",
    "font/ttf",
    "font/otf",
    "font/opentype",
    "image/svg+xml",
    "text/css",
    "text/csv",
    "text/html",
    "text/javascript",
    "text/js",
    "text/plain",
    "text/richtext",
    "text/tab-separated-values",
    "text/xml",
    "text/x-script",
    "text/x-component",
    "text/x-java-source",
  ]

  origin_host_header  = module.storage.primary_blob_host
  origin {
    name              = "origin"
    host_name         = module.storage.primary_blob_host
  }

  tags                = local.tags
}

resource "azurerm_dns_cname_record" "this" {
  name                = local.subdomain_base
  zone_name           = data.azurerm_dns_zone.this.name
  resource_group_name = data.azurerm_dns_zone.this.resource_group_name
  ttl                 = var.dns_ttl
  target_resource_id  = azurerm_cdn_endpoint.this.id
  tags                = local.tags
}

resource "cloudflare_record" "this" {
  count   = var.domain_on_cloudflare ? 1 : 0

  zone_id = data.cloudflare_zones.zone.0.zones.0.id
  name    = var.subdomain
  value   = azurerm_cdn_endpoint.this.fqdn
  type    = "CNAME"
  proxied = false
}

resource "time_sleep" "wait_30_seconds" {
  create_duration   = "30s"
  destroy_duration  = "30s"

  depends_on        = [
    azurerm_dns_cname_record.this,
  ]
}

resource "azurerm_cdn_endpoint_custom_domain" "this" {
  name                = local.subdomain_base
  cdn_endpoint_id     = azurerm_cdn_endpoint.this.id
  host_name           = "${azurerm_dns_cname_record.this.name}.${data.azurerm_dns_zone.this.name}"

  cdn_managed_https {
    certificate_type  = "Dedicated"
    protocol_type     = "ServerNameIndication"
    tls_version       = "TLS12"
  }

  depends_on          = [
    time_sleep.wait_30_seconds, # Prevent any crashes if the DNS hasn't been propagated
  ]
}
